# arxivdl
![arxivdl at work](examples/downloading.png "arxivdl")
**arxivdl** is a tool for downloading [arxiv](https://www.arxiv.org) papers with *real titles* as file names. 
## Motivation
Papers downloaded from arxiv by default have awkward names, like `1234.12345.pdf`. Usually you would want the file name to be like the actual title of the webpage, like `[1234.12345] A New Way of Naming Files.pdf`. This is what **arxivdl** does: it downloads papers with their actual titles as file names. No need to change these later.
Also, **arxivdl** can automatically rename pdfs for you if you've already downloaded some from arxiv. This can be done one pdf at a time, or for an entire directory at once.
## Usage
1. Downloading a paper from arxiv with title as filename: `arxivdl https://www.arxiv.org/abs/1234.12345 ~/my_dir/`

2. Renaming existing pdf: `arxivdl -pdf ~/my_dir/1234.12345.pdf`

3. Renaming all existing pdfs in a single directory: `arxivdl -dir ~/my_dir/`

## Dependencies
See `Cargo.toml`
## License
See `LICENSE`
## Examples
![arxivdl at work](examples/renaming_dir.png "arxivdl")