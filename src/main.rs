extern crate select;
extern crate ureq;
extern crate tendril;

use std::env;
use std::fs;
use std::io::prelude::*;
use std::path::PathBuf;
use tendril::format_tendril;
use select::document::Document;
use select::predicate::Name;


fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 3 {
        print_help();
        return;
    }

    let path = PathBuf::from(args[2].to_string());
    let url = PathBuf::from(args[1].to_string());
    
    match args[1].as_ref() {
        "-h" => print_help(),
        "-dir" => dir_titler(path),
        "-pdf" => pdf_titler(path),
        _ => download(url, path),
    }
}

fn print_help() {
    println!("Usage: arxivdl https://www.arxiv.org/abs/xxxx.xxxxx ~/user/dir/");
}

fn dir_titler(dir_path: PathBuf) {
    if let Ok(entries) = fs::read_dir(&dir_path) {
        for entry in entries {
            if let Ok(entry) = entry {
                pdf_titler(entry.path());
            }
        }
    } else {
        println!("Invalid path '{}'.", dir_path.to_str().unwrap());
    }
}

fn pdf_titler(pdf_path: PathBuf) {
    if  !pdf_path.extension().is_none() && !pdf_path.file_stem().is_none() && pdf_path.extension().unwrap().to_str().unwrap().to_lowercase() == "pdf" {
        let new_title = id_to_title(pdf_path.file_stem().unwrap().to_str().unwrap().into());
        if pdf_path.file_name().unwrap().to_str().unwrap() != new_title {
            if fs::rename(&pdf_path, &pdf_path.with_file_name(&new_title)).is_ok() {
                println!("Rename '{}' as '{}'", &pdf_path.file_name().unwrap().to_str().unwrap(), &pdf_path.with_file_name(&new_title).file_name().unwrap().to_str().unwrap());
            }
        }
    }
}

fn id_to_title(id: String) -> String {
    let url = format!("https://arxiv.org/abs/{}", id);
    let old_title = format!("{}.pdf", id);
    
    let resp = ureq::get(&url)
        .timeout_connect(5_000)
        .call();

    if resp.status() == 200 {
        let body = resp.into_string().unwrap();
        let document = Document::from(format_tendril!("{}", body));

        for node in document.find(Name("title")).take(1) {
            return format!("{}.pdf", node.text());
        }
    }

    return old_title;
}

fn download(url: PathBuf, dir_path: PathBuf) {
    if dir_path.is_dir() {

        let file_id;
        if url.extension().is_none() {
            println!("Invalid url '{}'.", url.to_str().unwrap());
            return;
        }
        if url.extension().unwrap().to_str().unwrap().to_string() == "pdf" {
            file_id = url.file_stem().unwrap().to_str().unwrap().to_string();
        } else {
            file_id = url.file_name().unwrap().to_str().unwrap().to_string();
        }

        let file_url = format!("https://arxiv.org/pdf/{}.pdf", file_id);

        let resp = ureq::get(&file_url)
            .timeout_connect(5_000)
            .call();

        if resp.status() == 200 {
            let file_id = url.file_name().unwrap().to_str().unwrap().to_string();
            let real_title = id_to_title(file_id);

            let file_name = format!("{}/{}",
                dir_path
                    .to_str().
                    unwrap().
                    to_string(),
                real_title);

            let file = fs::File::create(file_name);
            let mut reader = resp.into_reader();
            let mut bytes = vec![];
            reader.read_to_end(&mut bytes).unwrap();


            if file.is_ok() {
                let mut f = file.unwrap();
                if f.write_all(&bytes).is_ok() {
                    println!("Downloaded '{}'", real_title);
                }
            }
        } else {
            println!("Could not retrieve document");
        }

    } else {
        println!("Invalid path '{}.", dir_path.to_str().unwrap());
    }
}